<?php
class Mage_Vpos_Model_Est_Source_PaymentAction
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => Mage_Vpos_Model_Est::ACTION_AUTHORIZE_CAPTURE,
                'label' => Mage::helper('vpos')->__('Hemen Çekim')
            ),
            array(
                'value' => Mage_Vpos_Model_Est::ACTION_AUTHORIZE,
                'label' => Mage::helper('vpos')->__('Kart limitine bloke')
            ),
        );
    }
}