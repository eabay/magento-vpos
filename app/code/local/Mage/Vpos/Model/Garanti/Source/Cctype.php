<?php
class Mage_Vpos_Model_Garanti_Source_Cctype extends Mage_Payment_Model_Source_Cctype
{
    public function getAllowedTypes()
    {
        return array('VI', 'MC', 'AE', 'DC');
    }
}