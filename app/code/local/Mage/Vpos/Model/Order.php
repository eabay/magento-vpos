<?php
class Mage_Vpos_Model_Order extends Mage_Sales_Model_Order
{
  protected $vpos_info;

	protected function _afterSave()
  {
    parent::_afterSave();
    
		$payment = $this->getPayment();
    
    $this->vpos_info = Mage::getModel('vpos/order_payment_instalment');
    
		$this->vpos_info
      ->setParentId($this->getId())
      ->setInstalment($payment->getInstalment())
      ->save();

    return $this;
  }

  public function _afterLoad()
  {
  	parent::_afterLoad();
    
  	$vpos_info_collection = Mage::getResourceModel('vpos/order_payment_instalment_collection');
  	$vpos_info_collection
      ->setOrderFilter($this->getId())
			->addAttributeToSelect('*')
			->load();

  	foreach ($vpos_info_collection as $vpos) {
  		// Take the first payment, ignore the rest (it's just one in theory)
  		$this->vpos_info = $vpos;
  		break;
  	};
  }

  public function getPayment()
  {
    $setup = Mage::getModel('vpos/entity_setup');
    $setup->installEntities($setup->getDefaultEntities()); 

    $payment = parent::getPayment();

    if ($payment && $this->vpos_info) {
    	$payment->addData($this->vpos_info->getData());
    }
    
	  return $payment;
  }
}
?>
