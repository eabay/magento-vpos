<?php
class Mage_Vpos_Model_Est extends Mage_Payment_Model_Method_Cc
{
    const HOST = 'https://vpos.est.com.tr/servlet/cc5ApiServer';

    const CHARGE_TYPE_AUTH     = 'Auth';
    const CHARGE_TYPE_PREAUTH  = 'PreAuth';
    const CHARGE_TYPE_POSTAUTH = 'PostAuth';
    const CHARGE_TYPE_CREDIT   = 'Credit';
    const CHARGE_TYPE_VOID     = 'Void';
    
    const RESPONSE_CODE_APPROVED = 'Approved';
    const RESPONSE_CODE_DECLINED = 'Declined';
    const RESPONSE_CODE_ERROR    = 'Error';

    protected $_isGateway               = true;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = true;
    protected $_canRefund               = true;
    protected $_canVoid                 = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;
    protected $_canSaveCc               = false;
    
    protected $_formBlockType = 'vpos/form_est';
    
    public function assignData($data)
    {
        parent::assignData($data);
        
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setInstalment($data->getInstalment());
        return $this;
    }
    
    public function OtherCcType($type)
    {
        $otherCardTypes = array('OT', 'DC');
        
        return in_array($type, $otherCardTypes);
    }
    
    public function validateCcNumOther($ccNumber)
    {
        $info = $this->getInfoInstance();
        
        switch ($info->getCcType()) {
        	case 'DC':
        		return preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/', $ccNumber);
        		break;
        		
          case 'OT':
        	default:
        		return preg_match('/^\\d+$/', $ccNumber);
        		break;
        }
    }

    public function authorize(Varien_Object $payment, $amount)
    {
        $error = false;

        if($amount>0){
            $payment->setType(self::CHARGE_TYPE_PREAUTH);
            $payment->setTotal($amount);

            $request= $this->_buildRequest($payment);
            $result = $this->_postRequest($request);

            $payment->setCcApproval($result->getApprovalCode())
                ->setLastTransId($result->getTransactionId())
                ->setCcTransId($result->getTransactionId());

            switch ($result->getResponseCode()) {
                case self::RESPONSE_CODE_APPROVED:
                    $payment->setStatus(self::STATUS_APPROVED);
                    break;
                case self::RESPONSE_CODE_DECLINED:
                    $payment->setStatus(self::STATUS_DECLINED);
                    $error = Mage::helper('vpos')->__('Payment authorization transaction has been declined');
                    $error .= "\n --> ". $result->getResponseReasonText();
                    break;
                case self::RESPONSE_CODE_ERROR:
                    $payment->setStatus(self::STATUS_ERROR);
                    $error = Mage::helper('vpos')->__('Error in authorizing the payment');
                    $error .= "\n --> ". $result->getResponseReasonText();
                    break;
                default:
                    $payment->setStatus(self::STATUS_UNKNOWN);
                    $error = Mage::helper('vpos')->__($result->getResponseReasonText());
                    break;
            }
        }else{
            $error = Mage::helper('vpos')->__('Invalid amount for authorization.');
        }

        if ($error !== false) {
            Mage::throwException($error);
        }
        return $this;
    }

    public function capture(Varien_Object $payment, $amount)
    {
        $error = false;

        if ($payment->getCcTransId()) {
            $payment->setType(self::CHARGE_TYPE_POSTAUTH);
        } else {
            $payment->setType(self::CHARGE_TYPE_AUTH);
        }

        $payment->setTotal($amount);

        $request= $this->_buildRequest($payment);
        $result = $this->_postRequest($request);

        if ($result->getResponseCode() == self::RESPONSE_CODE_APPROVED) {
            $payment->setStatus(self::STATUS_APPROVED);
            //$payment->setCcTransId($result->getTransactionId());
            $payment->setLastTransId($result->getTransactionId());
        }
        else {
            if ($result->getResponseReasonText()) {
                $error = $result->getResponseReasonText();
            }
            else {
                $error = Mage::helper('vpos')->__('Error in capturing the payment '.$payment->getCcTransId());
            }
        }

        if ($error !== false) {
            Mage::throwException($error);
        }

        return $this;
    }

    public function void(Varien_Object $payment)
    {
        $error = false;
        if($payment->getVoidTransactionId()){
            $payment->setType(self::CHARGE_TYPE_VOID);
            $request = $this->_buildRequest($payment);
						$request->setTransactionId($payment->getVoidTransactionId());
            $result = $this->_postRequest($request);
            if($result->getResponseCode()==self::RESPONSE_CODE_APPROVED){
                 $payment->setStatus(self::STATUS_SUCCESS );
            }
            else{
                $payment->setStatus(self::STATUS_ERROR);
                $error = $result->getResponseReasonText();
            }
        }else{
            $payment->setStatus(self::STATUS_ERROR);
            $error = Mage::helper('vpos')->__('Invalid transaction id');
        }
        if ($error !== false) {
            Mage::throwException($error);
        }
        return $this;
    }

    public function refund(Varien_Object $payment, $amount)
    {
        $error = false;
        if ($payment->getRefundTransactionId() && $amount>0) {
            $payment->setChargeType(self::REQUEST_TYPE_CREDIT);
            $request = $this->_buildRequest($payment);
            $request->setTransId($payment->getRefundTransactionId());
            $result = $this->_postRequest($request);

            if ($result->getResponseCode()==self::RESPONSE_CODE_APPROVED) {
                $payment->setStatus(self::STATUS_SUCCESS);
            } else {
                $error = $result->getResponseReasonText();
            }

        } else {
            $error = Mage::helper('vpos')->__('Error in refunding the payment');
        }

        if ($error !== false) {
            Mage::throwException($error);
        }
        return $this;
    }

    protected function _buildRequest(Varien_Object $payment)
    {
        $order = $payment->getOrder();

        $request = Mage::getModel('vpos/est_request')
            ->setType($payment->getType())
            ->setMode('P')
            ->setIpAddress($order->getRemoteIp())
            ->setOrderId($order->getId())
            ->setEmail($order->getCustomerEmail());
            
        if($this->getConfigData('test')) {
          $request
            ->setName($this->getConfigData('test_apiname'))
            ->setPassword($this->getConfigData('test_apipass'))
            ->setClientId($this->getConfigData('test_apiclient'));
        } else {
          $request
            ->setName($this->getConfigData('apiname'))
            ->setPassword($this->getConfigData('apipass'))
            ->setClientId($this->getConfigData('apiclient'));
        }
        
        if($payment->getInstalment() && $payment->getInstalment() > 0)
        {
          $request->setInstalment($payment->getInstalment());
        }

        if($payment->getTotal()){
            $request->setTotal($payment->getTotal(),2);
            $request->setCurrency(constant("Mage_Vpos_Model_Est_Source_Currency::CURRENCY_ISO_CODE_".$order->getBaseCurrencyCode()));
        }
        
        switch ($payment->getType()) {
            case self::CHARGE_TYPE_POSTAUTH:
            case self::CHARGE_TYPE_CREDIT:
            case self::CHARGE_TYPE_VOID:
                $request->setTransactionId($payment->getCcTransId());
                break;
        }

        if (!empty($order)) {
            $request->setOrderId($order->getIncrementId());

            $billing = $order->getBillingAddress();
            if (!empty($billing)) {
                $request
                    ->setUserId($billing->getCustomerId())
                    ->setBName($billing->getFirstname() .' '.$billing->getLastname())
                    ->setBStreet1($billing->getStreet(1))
                    ->setBStreet2($billing->getStreet(2))
                    ->setBCity($billing->getCity())
                    ->setBStateProv($billing->getRegion())
                    ->setBPostalCode($billing->getPostcode())
                    ->setBCountry(constant("Mage_Vpos_Model_Est_Source_Country::COUNTRY_ISO_CODE_".$billing->getCountry()))
                    ->setBTelVoice($billing->getTelephone())
                    ->setBCompany($billing->getCompany());
            }

            $shipping = $order->getShippingAddress();
            if (!empty($shipping)) {
                $request
                    ->setSName($shipping->getFirstname() .' '.$shipping->getLastname())
                    ->setSStreet1($shipping->getStreet(1))
                    ->setSStreet2($shipping->getStreet(2))
                    ->setSCity($shipping->getCity())
                    ->setSStateProv($shipping->getRegion())
                    ->setSPostalCode($shipping->getPostcode())
                    ->setSCountry(constant("Mage_Vpos_Model_Est_Source_Country::COUNTRY_ISO_CODE_".$shipping->getCountry()));
            }
        }

        if($payment->getCcNumber()){
            $request
                ->setNumber($payment->getCcNumber())
                ->setExpires(sprintf('%02d/%02d', $payment->getCcExpMonth(), substr($payment->getCcExpYear(), -2)))
                ->setCvv2Val($payment->getCcCid());
        }

        return $request;
    }

    protected function _postRequest(Varien_Object $request)
    {
        $result = Mage::getModel('vpos/est_result');

        $client = new Varien_Http_Client();

        $uri = $this->getConfigData('host');
        $client->setUri($uri ? $uri : self::HOST);
        $client->setConfig(array(
            'maxredirects' => 0,
            'timeout' => 30,
        ));
        $client->setMethod(Zend_Http_Client::POST);
        $client->setHeaders('Content-type', 'text/xml');
        
        $info = $request->getData();
        
        $xw = new xmlWriter();
        $xw->openMemory();
        $xw->setIndent(true);
        $xw->setIndentString('  ');
        
        $xw->startDocument('1.0','UTF-8');
        
        $xw->startElement('CC5Request');
        $xw->writeElement('Name', $info['name']);
        $xw->writeElement('Password', $info['password']);
        $xw->writeElement('ClientId', $info['client_id']);
        $xw->writeElement('IPAddress', $info['ip_address']);
        $xw->writeElement('Email', $info['email']);
        $xw->writeElement('Mode', $info['mode']);
        $xw->writeElement('OrderId', $info['order_id']);
        $xw->writeElement('GroupId', '');
        $xw->writeElement('TransId', isset($info['transaction_id']) ? $info['transaction_id'] : '');
        $xw->writeElement('UserId', $info['user_id']);
        $xw->writeElement('Type', $info['type']);
        $xw->writeElement('Number', $info['number']);
        $xw->writeElement('Expires', $info['expires']);
        $xw->writeElement('Cvv2Val', $info['cvv2_val']);
        $xw->writeElement('Total', $info['total']);
        $xw->writeElement('Currency', $info['currency']);
        $xw->writeElement('Taksit', isset($info['instalment']) ? $info['instalment'] : '');
        $xw->startElement('BillTo');
        $xw->writeElement('Name', $info['b_name']);
        $xw->writeElement('Street1', $info['b_street1']);
        $xw->writeElement('Street2', $info['b_street2']);
        $xw->writeElement('Street3', '');
        $xw->writeElement('City', $info['b_city']);
        $xw->writeElement('StateProv', $info['b_state_prov']);
        $xw->writeElement('PostalCode', $info['b_postal_code']);
        $xw->writeElement('Country', $info['b_country']);
        $xw->writeElement('Company', $info['b_company']);
        $xw->writeElement('TelVoice', $info['b_tel_voice']);
        $xw->endElement(); //BillTo
        $xw->startElement('ShipTo');
        $xw->writeElement('Name', $info['s_name']);
        $xw->writeElement('Street1', $info['s_street1']);
        $xw->writeElement('Street2', $info['s_street2']);
        $xw->writeElement('Street3', '');
        $xw->writeElement('City', $info['s_city']);
        $xw->writeElement('StateProv', $info['s_state_prov']);
        $xw->writeElement('PostalCode', $info['s_postal_code']);
        $xw->writeElement('Country', $info['s_country']);
        $xw->endElement(); //ShipTo
        $xw->endElement(); //CC5Request
        
        $xml = $xw->outputMemory(true);
        
        $client->setParameterPost('DATA', $xml);

        if ($this->getConfigData('debug')) {

            $debug = Mage::getModel('vpos/est_debug')
                ->setRequestBody($xml)
                ->setRequestSerialized(serialize($request->getData()))
                ->setRequestDump(print_r($request->getData(),1))
                ->save();
        }

        try {
            $response = $client->request();
        } catch (Exception $e) {
            $result->setResponseCode(-1)
                ->setResponseReasonCode($e->getCode())
                ->setResponseReasonText($e->getMessage());

            if (!empty($debug)) {
                $debug
                    ->setResultSerialized(serialize($result->getData()))
                    ->setResultDump(print_r($result->getData(),1))
                    ->save();
            }
            Mage::throwException(
                Mage::helper('vpos')->__('Gateway request error: %s', $e->getMessage())
            );
        }

        $responseBody = $response->getBody();

        $r = simplexml_load_string($responseBody);

        if ($r) {
            $result->setResponseCode($r->Response)
            ->setResponseReasonCode($r->ProcReturnCode)
            ->setResponseReasonText($r->ErrMsg . ' / '. $r->Extra->HOSTMSG)
            ->setOrderId($r->OrderId)
            ->setGroupId($r->GroupId)
            ->setAuthCode($r->AuthCode)
            ->setTransactionId($r->TransId)
            ->setTrxDate($r->Extra->TRXDATE)
            ->setNumCode($r->Extra->NUMCODE);
        } else {
             Mage::throwException(
                Mage::helper('vpos')->__('Error in payment gateway')
            );
        }

        if (!empty($debug)) {
            $debug
                ->setResponseBody($responseBody)
                ->setResultSerialized(serialize($result->getData()))
                ->setResultDump(print_r($result->getData(),1))
                ->save();
        }

        return $result;
    }
}
