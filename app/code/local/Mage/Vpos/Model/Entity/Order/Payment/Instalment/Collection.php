<?php
class Mage_Vpos_Model_Entity_Order_Payment_Instalment_Collection extends Mage_Eav_Model_Entity_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('vpos/order_payment_instalment');
    }

    public function setOrderFilter($orderId)
    {
        $this->addAttributeToFilter('parent_id', $orderId);
        return $this;
    }

}

?>
