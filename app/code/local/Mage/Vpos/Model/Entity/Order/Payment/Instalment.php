<?php

 class Mage_Vpos_Model_Entity_Order_Payment_Instalment extends Mage_Eav_Model_Entity_Abstract
{

    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('vpos_instalment');
        $read = $resource->getConnection('vpos_setup');
        $write = $resource->getConnection('vpos_write');
        $this->setConnection($read, $write);
    }

    public function setOrderFilter($orderId)
    {
        $this->addAttributeToFilter('parent_id', $orderId);
        return $this;
    }
}
?>
