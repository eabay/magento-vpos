<?php
class Mage_Vpos_Model_Entity_Setup extends Mage_Eav_Model_Entity_Setup
{
  public function getDefaultEntities()
  {
    return array(
  	  'vpos_instalment' => array(
        'entity_model' => 'vpos/order_payment_instalment',
        'table' => 'sales/order_entity',
        'attributes' => array(
          'parent_id' => array(
            'type' => 'static',
            'label' => 'Taksit',
            'backend' => 'sales_entity/order_attribute_backend_child'
          ),
          'instalment' => array('type' => 'int')
        )
      )
    );
  }
}
?>
