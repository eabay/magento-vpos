<?php
class Mage_Vpos_Block_Form_Est extends Mage_Payment_Block_Form_Cc
{
  protected function _construct()
  {
    parent::_construct();
    $this->setTemplate('payment/form/est.phtml');
  }
  
  public function getInstalments()
  {
    if ($method = $this->getMethod()) {
      $instalments = $method->getConfigData('instalments');
    }
    return $instalments;
  }
  
  public function getGrandTotal()
  {
    return Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();
  }

  public function formatPrice($price)
  {
    return Mage::getSingleton('checkout/session')->getQuote()->getStore()->formatPrice($price);
  }
}