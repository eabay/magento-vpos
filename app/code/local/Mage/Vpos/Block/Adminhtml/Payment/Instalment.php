<?php
class Mage_Vpos_Block_Adminhtml_Payment_Instalment extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
  public function __construct()
  {
    $this->addColumn('instalment', array(
      'label' => Mage::helper('adminhtml')->__('Taksit Sayısı'),
      'size'  => 15,
    ));
    $this->addColumn('commission', array(
      'label' => Mage::helper('adminhtml')->__('Komisyon Oranı (%)'),
      'size'  => 18
    ));
    $this->_addButtonLabel = Mage::helper('adminhtml')->__('Taksit Ekle');
    parent::__construct();
  }
}